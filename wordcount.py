import sys
import re
from collections import Counter


def print_words(filename):
    print("print_words")
    word_dict = count_words(filename)
    for key in sorted(word_dict):
        print(key, word_dict[key])


def print_top(filename):
    print("print_top")
    word_dict = count_words(filename)
    # Sorts the dictionary by largest value first
    word_dict = {k: v for k, v in sorted(word_dict.items(), key=lambda item: item[1], reverse=True)}
    # prints first 20
    for idx, (key, value) in enumerate(word_dict.items()):
        if idx == 20:
            break
        print(key, value)



def count_words(filename):
    """count_words(filename (A string))
    Returns a list, with words and number of appearances"""
    with open(filename, 'r') as file:
        word_dict = Counter()
        for line in file:
            res = re.findall("[\w]+[\'][\w]|[\w]+", line.lower()) # Changes to lower, then adds only a-z sequences, and ones with ' in them
            word_dict.update(res)
    return word_dict


def main():
    if len(sys.argv) != 3:
        print("usage: ./wordcount.py {--count | ---topcount} file")
        sys.exit(1)

    option = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)

    else:
        print("unknown option: " + option)
        sys.exit(1)

if __name__ == '__main__':
    main()
